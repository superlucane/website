---
layout: post
title:  "Benvenuti al cane"
date:   2020-05-12 18:15:55 +0300
image:  cane03.jpg
tags:   cane
---

Ciao a tutti, sono ancora **il cane**.

Cane cane, solo cane... se tu tocchi il cane non potrai mangiare il pane, saltano le rane che tornano nelle tane mangiando banane avvolte nel salame.
